export function checkboxAction(node: HTMLElement, slug: string): { destroy: () => void } {
    function updateStorage(event: Event): void {
        const target = event.target as HTMLInputElement;
        const checkboxId = target.id;
        console.log("aaaa" + checkboxId + target.checked);
        localStorage.setItem(checkboxId, String(target.checked));
    }

    node.addEventListener('change', updateStorage);

    return {
        destroy() {
            node.removeEventListener('change', updateStorage);
        }
    };
}
