/** @type {import('./$types').PageLoad} */
import fs from 'fs';
import path from 'path';
import { browser } from "$app/environment";

type LoadInput = {
    params: {
        slug: string;
    };
    // 他の必要なプロパティもここに追加できます
};
export async function load({ params }: LoadInput) {
    if (!browser) {
        const { slug } = params;

        const markdownFilePath = path.join(process.cwd(), `./src/contents/${slug}.md`);
        const markdownContent = fs.readFileSync(markdownFilePath, 'utf-8');

        return {
            props: {
                markdownContent,
                slug,
            },
        };
    }
}

